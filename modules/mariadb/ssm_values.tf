resource "aws_ssm_parameter" "db_host" {
  count = "${var.store_in_ssm}" ? 1 : 0
  name = "${var.env}-${var.db_name}-db-host"
  type = "String"
  value = "jdbc:mysql://${aws_db_instance.db.address}:3306/${var.db_name}"
  tags = {
    STAGE = "${var.env}"
    CreatedBy = "Terraform"
  }
}

resource "aws_ssm_parameter" "db_secret" {
  count = "${var.store_in_ssm}" ? 1 : 0
  name = "${var.env}-${var.db_name}-db-secret"
  type = "String"
  value = "${aws_secretsmanager_secret.db_admin_secret.name}"
  tags = {
    STAGE = "${var.env}"
    CreatedBy = "Terraform"
  }
}
