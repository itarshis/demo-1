# Derived

variable "env" {}

variable "db_name" {}

variable "db_class" {}

variable "db_size" {
  type = number
  default = 20
}

variable "db_version" {
  type = string
  default = null
}

variable "db_backup_retention" {
  type = number
  default = 14
}

variable "db_admin_user" {
  type = string
  default = "dbadmin"
}

variable "db_sg_ids" {
  type = list(string)
  default = null
}

variable "db_subnet_group" {}

variable "store_in_ssm" {
  type = bool
  default = false
}

# AWS Environment

data "aws_availability_zones" "available" {}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}
