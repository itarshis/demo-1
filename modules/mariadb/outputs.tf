# Output VPC info for consumption by other configs

output "info" {
	value = {
		db_arn = "${aws_db_instance.db.arn}"
		db_name = "${aws_db_instance.db.name}"
		db_host = "${aws_db_instance.db.address}"
		db_user = "${aws_db_instance.db.username}"
		db_secret = "${aws_secretsmanager_secret.db_admin_secret.arn}"
		db_secret_name = "${aws_secretsmanager_secret.db_admin_secret.name}"
		db_sm_kms_key = "${aws_kms_key.sm_key.arn}"
		db_rds_kms_key = "${aws_kms_key.db_key.arn}"
	}
}
