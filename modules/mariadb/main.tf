#Generate SQL DB user password
resource "random_string" "db_admin_pwd" {
  length = 16
  special = true
  override_special = "!$"
}

# Create key for encrypting database and secrets

resource "aws_kms_key" "db_key" {
  description = "KMS key for encrypting RDS DBs"
  tags = {
    Name = "${var.env}-${var.db_name}-rds-encryption-key"
    CreatedBy = "Terraform"
    STAGE = "${var.env}"
  }
}

resource "aws_kms_key" "sm_key" {
  description = "KMS key for encrypting secrets in Secrets Manager"
  tags = {
    Name = "${var.env}-${var.db_name}-sm-encryption-key"
    CreatedBy = "Terraform"
    STAGE = "${var.env}"
  }
}

# Store RDS sysadmin credentials in Secrets Manager

resource "aws_secretsmanager_secret" "db_admin_secret" {
  name = "${var.env}-${aws_db_instance.db.identifier}-db-creds"
  kms_key_id = "${aws_kms_key.sm_key.id}"
  tags = {
    STAGE = "${var.env}"
    CreatedBy = "Terraform"
  }
}

resource "aws_secretsmanager_secret_version" "db_admin_secret" {
  secret_id = "${aws_secretsmanager_secret.db_admin_secret.id}"
  secret_string = "${random_string.db_admin_pwd.result}"
}

# Create an RDS database instance

resource "aws_db_instance" "db" {
  allocated_storage = "${var.db_size}"
  storage_type = "gp2"
  engine = "mariadb"
  engine_version = "${var.db_version}"
  instance_class = "${var.db_class}"
  name = "${var.db_name}"
  identifier = "${var.db_name}"
  username = "${var.db_admin_user}"
  password = "${random_string.db_admin_pwd.result}"
  backup_retention_period = "${var.db_backup_retention}"
  multi_az = "${var.env == "prod" ? "true" : "false"}"
  storage_encrypted = true
  kms_key_id = "${aws_kms_key.db_key.arn}"
  db_subnet_group_name = "${var.db_subnet_group}"
  vpc_security_group_ids = "${var.db_sg_ids}"
  final_snapshot_identifier = "${var.env}-${var.db_name}-final-${formatdate("MMDDYYYY", timestamp())}"
  tags = {
    STAGE = "${var.env}"
    CreatedBy = "Terraform"
  }
}
