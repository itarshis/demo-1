resource "aws_ebs_default_kms_key" "ebs_kms" {
  key_arn = "${var.ebs_kms}"
}

resource "aws_launch_configuration" "server" {
  name_prefix = "${var.env}-${var.app}-lc"
  image_id = "${var.image_id}"
  instance_type = "${var.ec2_type}"
  #user_data = data.template_file.userdata.rendered
  user_data = templatefile("${path.module}/files/userdata.tpl", {
    web_text = "${var.web_text}"
  })
  key_name = var.ssh_key
  enable_monitoring = true
  security_groups = "${var.sgs}"
  iam_instance_profile = "${aws_iam_instance_profile.profile.name}"
  root_block_device {
    volume_type = "gp2"
    volume_size = "${var.volume_size}"
    encrypted = false
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "server" {
  name = "${aws_launch_configuration.server.name}-asg"
  launch_configuration = "${aws_launch_configuration.server.name}"
  min_size = 1
  max_size = 1
  vpc_zone_identifier = split(",",element(var.asg_vpc_zones,0))
  target_group_arns = "${var.target_group_arn}"
  lifecycle {
    create_before_destroy = true
  }
  tag {
    key = "Name"
    value = "${var.env}-${var.asg_name}"
    propagate_at_launch = true
  }
  tag {
    key = "CreatedBy"
    value = "Terraform"
    propagate_at_launch = false
  }
  tag {
    key = "STAGE"
    value = var.env
    propagate_at_launch = true
  }
}
