variable "env" {
  type = string
  default = ""
}

variable "app" {
  type = string
  default = null
}

variable "web_text" {
  type = string
  default = "Hey You!"
}

variable "ssh_key" {
  type = string
  default = ""
}

variable "ebs_kms" {
  type = string
  default = ""
}

variable "sm_kms" {
  type = string
  default = ""
}

variable "s3_kms" {
  type = string
  default = ""
}

variable "volume_size" {
  type = number
}

variable "sgs" {
  type = list(string)
  default = [""]
}

variable "image_id" {
  type = string
  default = ""
}

variable "asg_name" {
  type = string
  default = ""
}

variable "asg_vpc_zones" {}

variable "target_group_arn" {}

variable "ec2_type" {
  default = "t2.micro"
}

variable "scanned_bucket_arn" {
  type = string
  default = ""
}

data "template_file" "userdata" {
  template = "${file("${path.module}/files/userdata.tpl")}"
  vars = {
    web_text = "${var.web_text}"
  }
}

data "aws_region" "current" {}
