#!/bin/bash

# Set some basic variables based on instance metadata
az=`curl http://169.254.169.254/latest/meta-data/placement/availability-zone`
private_ip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
region=$${az::-1}

# Configure logging so that logs are sent to the correct region
awslogs_cli_file = /etc/awslogs/awscli.conf
sed -i -e "s/region = us-east-1/region = $region/g" $awslogs_cli_file

yum install httpd -y

listen $private_ip:80

touch /var/www/html/index.html

echo "<h1>${web_text}</h1>" > /var/www/html/index.html

service httpd restart
