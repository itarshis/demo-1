resource "aws_kms_key" "s3_encryption" {
  description = "KMS key for encrypting S3 buckets"
  tags = {
    CreatedBy = "Terraform"
    STAGE = "${var.env}"
  }
}

resource "aws_kms_key" "secrets_manager_encryption" {
  description = "KMS key for encrypting secrets in Secrets Manager"
  tags = {
    CreatedBy = "Terraform"
    STAGE = "${var.env}"
  }
}

resource "aws_kms_key" "ebs_volume_encryption" {
  description = "KMS key for encrypting secrets in EBS volumes"
  tags = {
    CreatedBy = "Terraform"
    STAGE = "${var.env}"
  }
}

resource "aws_kms_key" "dynamodb_encryption" {
  description = "KMS key for encrypting DynamoDB tables"
  tags = {
    CreatedBy = "Terraform"
    STAGE = "${var.env}"
  }
}
