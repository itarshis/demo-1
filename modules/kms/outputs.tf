# Output VPC info for consumption by other configs

output "export" {
	value = {
		s3_kms_arn = "${aws_kms_key.s3_encryption.arn}"
		sm_kms_arn = "${aws_kms_key.secrets_manager_encryption.arn}"
		ebs_kms_arn = "${aws_kms_key.ebs_volume_encryption.arn}"
		ddb_kms_arn = "${aws_kms_key.dynamodb_encryption.arn}"
		s3_kms_id = "${aws_kms_key.s3_encryption.id}"
		sm_kms_id = "${aws_kms_key.secrets_manager_encryption.id}"
		ebs_kms_id = "${aws_kms_key.ebs_volume_encryption.id}"
		ddb_kms_id = "${aws_kms_key.dynamodb_encryption.id}"
	}
}
