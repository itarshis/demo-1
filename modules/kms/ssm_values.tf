resource "aws_ssm_parameter" "standard" {
    name = "${var.env}-srce-s3-kms"
    type = "String"
    value = "${aws_kms_key.s3_encryption.arn}"
}
