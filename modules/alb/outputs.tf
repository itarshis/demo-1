output "info" {
  value = {
    listener_http  = aws_alb_listener.http.arn
    listener_https = aws_alb_listener.https.arn
    alb_dns        = aws_alb.alb.dns_name
    alb_zone_id    = aws_alb.alb.zone_id
    target_group_arn = aws_alb_target_group.app.arn
  }
}
