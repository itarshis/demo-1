# Create a new load balancer
resource "aws_alb" "alb" {
  name            = "${var.name}" == "" ? "${var.env}" : "${var.name}"
  internal        = false
  security_groups = "${var.alb_sg}"

  # var.vpc["subnet_ids_public"] was stringified to be used as an output, so we must split it
  subnets = split(",", var.vpc["subnet_ids_public"])

  # Logs go to S3://bucket/AWSLogs/aws-account-id/elasticloadbalancing/
  access_logs {
    bucket  = "${var.log_bucket}"
    enabled = false
  }

  tags = {
    VPC         = "${var.vpc["vpc_name"]}"
    Environment = "${var.env}"
    CreatedBy   = "terraform"
  }
}

resource "aws_alb_listener" "http" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.app.arn}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "https" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = "443"
  protocol          = "HTTPS"

  # ssl_policy configuration:
  # https://docs.aws.amazon.com/elasticloadbalancing/latest/application/create-https-listener.html#describe-ssl-policies
  ssl_policy      = "ELBSecurityPolicy-2016-08"
  certificate_arn = "${aws_acm_certificate_validation.cert.certificate_arn}"

  default_action {
    target_group_arn = "${aws_alb_target_group.app.arn}"
    type = "forward"
  }
}

resource "aws_route53_record" "alb" {
  zone_id = "${var.dns_zone_id}"
  name = "${var.alb_record_name}"
  type = "A"
  alias {
    name = "${aws_alb.alb.dns_name}"
    zone_id = "${aws_alb.alb.zone_id}"
    evaluate_target_health = false
  }
}
