#-------------------------------------------------
# Inputs
#-------------------------------------------------

variable "env" {}

variable "app" {}

variable "vpc" {
  type = map(string)
}

variable "name" {
  default = ""
}

variable "log_bucket" {
  default = "demo-logs"
}

variable "alb_sg" {
  type = list(string)
}

variable "dns_zone_id" {}

variable "alb_record_name" {}

variable "has_service" {
  default = false
}

variable "web_path" {
  default = "/index.html"
}

#-------------------------------------------------
# Data Sources
#-------------------------------------------------

# retrieve aws account id
data "aws_caller_identity" "current" {}
