resource "aws_alb_target_group" "app" {
  name     = "${var.app}-${var.env}"
  port     = 80
  protocol = "HTTP"
  stickiness {
    type    = "lb_cookie"
    enabled = false
  }
  vpc_id = "${var.vpc["id"]}"

  health_check {
    path                = "${var.web_path}"
    interval            = 15
    timeout             = 5
    healthy_threshold   = "2"
    unhealthy_threshold = "5"
  }

  tags = {
    VPC           ="${var.vpc["vpc_name"]}"
    Environment   = "${var.env}"
    ApplicationID = "${var.app}"
    CreatedBy     = "terraform"
  }
}
