resource "random_string" "s3_tail" {
  length = 5
  special = false
  upper = false
}

# Create S3 Bucket using our random string to ensure that it's unique

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${var.env}-${var.bucket_name}-${random_string.s3_tail.result}"
  acl    = "private"
  tags = {
    Environment = "${var.env}"
    CreatedBy   = "Terraform"
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD"]
    expose_headers = ["ETag"]
    allowed_origins = ["*"]
  }
}
