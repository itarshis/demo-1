output "info" {
  value = {
    bucket_id  = aws_s3_bucket.s3_bucket.id
    bucket_arn = aws_s3_bucket.s3_bucket.arn
    bucket_dn = aws_s3_bucket.s3_bucket.bucket_domain_name
  }
}
