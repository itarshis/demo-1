resource "aws_ssm_parameter" "standard" {
    count = var.store_in_ssm ? 1 : 0
    name = "${var.env}-${var.bucket_name}-s3-bucket"
    type = "String"
    value = "${aws_s3_bucket.s3_bucket.id}"
}
