variable "env" {}

variable "bucket_name" {}

variable "store_in_ssm" {
  default = false
}

variable "use_kms" {
  default = false
}

data "aws_caller_identity" "current" {}
