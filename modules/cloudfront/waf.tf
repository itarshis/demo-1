resource "aws_waf_ipset" "ipset" {
  name = "srce-${var.env}-${var.site_name}-ipset"
  dynamic ip_set_descriptors {
    for_each = "${var.waf_ip_whitelist}"
    content {
      value = ip_set_descriptors.value["value"]
      type = ip_set_descriptors.value["type"]
    }
  }
}

resource "aws_waf_rule" "wafrule" {
  depends_on = [aws_waf_ipset.ipset]
  name = "${var.env}-${var.site_name}-srcewafrule"
  metric_name = "srcewafrule"

  predicates {
    data_id = aws_waf_ipset.ipset.id
    negated = false
    type = "IPMatch"
  }

  tags = {
    Environment = "${var.env}"
    CreatedBy   = "Terraform"
  }
}

resource "aws_waf_web_acl" "wafacl" {
  depends_on = [aws_waf_ipset.ipset, aws_waf_rule.wafrule]
  name = "${var.env}-srcewafacl-${var.site_name}"
  metric_name = "srcewafacl"

  default_action {
    type = "BLOCK"
  }

  rules {
    action {
      type = "ALLOW"
    }

    priority = 1
    rule_id = aws_waf_rule.wafrule.id
    type = "REGULAR"
  }
}
