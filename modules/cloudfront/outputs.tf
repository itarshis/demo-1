# Output VPC info for consumption by other configs

output "export" {
	value = {
    domain_name = aws_cloudfront_distribution.www-fe.domain_name
		origin_access_id_iam_arn = aws_cloudfront_origin_access_identity.standard.iam_arn
	}
}
