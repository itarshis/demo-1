# Deploy a CloudFront distribution in front of an S3 bucket

resource "aws_cloudfront_distribution" "www-fe" {
  origin {
    domain_name = "${var.s3_bucket}"
    origin_id = "${var.env}-${var.site_name}-origin"
    s3_origin_config {
      origin_access_identity = "${aws_cloudfront_origin_access_identity.standard.cloudfront_access_identity_path}"
    }

  }

  enabled             = true
  is_ipv6_enabled     = true
  aliases = ["${var.site_name}.${var.dns_domain}"]
  comment             = ""
  default_root_object = "${var.root_object}"
  web_acl_id = "${aws_waf_web_acl.wafacl.id}"

  #logging_config {
  #  include_cookies = false
  #  bucket          = "mylogs.s3.amazonaws.com"
  #  prefix          = "srce-${var.env}-${var.app}"
  #}

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${var.env}-${var.site_name}-origin"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations        = ["US", "CA"]
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = false
    acm_certificate_arn = "${aws_acm_certificate.cert.arn}"
    ssl_support_method = "sni-only"
  }
  depends_on = [
    aws_cloudfront_origin_access_identity.standard,
    aws_acm_certificate_validation.cert

  ]
  lifecycle {
    ignore_changes = [
      ordered_cache_behavior.0.lambda_function_association,
      default_cache_behavior.0.lambda_function_association,
      viewer_certificate
    ]
  }
}
