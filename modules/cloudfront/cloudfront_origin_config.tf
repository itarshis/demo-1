resource "aws_cloudfront_origin_access_identity" "standard" {
  comment = "Used for access to origins for Cloudfront Distributions"
}


data "aws_iam_policy_document" "s3_policy" {
 statement {
   actions   = ["s3:GetObject"]
   resources = ["${var.s3_bucket_arn}/*"]

   principals {
     type        = "AWS"
     identifiers = ["${aws_cloudfront_origin_access_identity.standard.iam_arn}"]
   }
 }

 statement {
   actions   = ["s3:ListBucket","s3:GetBucketAcl","s3:PutBucketAcl","s3:*"]
   resources = ["${var.s3_bucket_arn}"]

   principals {
     type        = "AWS"
     identifiers = ["${aws_cloudfront_origin_access_identity.standard.iam_arn}"]
   }
 }
 depends_on = [
   aws_cloudfront_origin_access_identity.standard
 ]
}

resource "aws_s3_bucket_policy" "example" {
  bucket = "${var.s3_bucket_id}"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "AWS": "${aws_cloudfront_origin_access_identity.standard.iam_arn}"
            },
            "Action": "s3:GetObject",
            "Resource": "${var.s3_bucket_arn}/*"
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "AWS": "${aws_cloudfront_origin_access_identity.standard.iam_arn}"
            },
            "Action": [
                "s3:PutBucketAcl",
                "s3:ListBucket",
                "s3:GetBucketAcl",
                "s3:*"
            ],
            "Resource": "${var.s3_bucket_arn}"
        }
    ]
}
POLICY
}
