resource "aws_acm_certificate" "cert" {
  domain_name       = "${var.site_name}.${var.dns_domain}"
  validation_method = "DNS"

  tags = {
    Environment = "${var.env}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = "${aws_acm_certificate.cert.arn}"
  validation_record_fqdns = [join(",", "${aws_route53_record.cert_validation.*.fqdn}")]
}

resource "aws_route53_record" "cert_validation" {
  name    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_type}"
  zone_id = "${data.aws_route53_zone.dns_domain.zone_id}"
  records = ["${aws_acm_certificate.cert.domain_validation_options.0.resource_record_value}"]
  ttl     = 60
  depends_on = [
    aws_acm_certificate.cert
  ]
}

resource "aws_route53_record" "cloudfront" {
  zone_id = "${data.aws_route53_zone.dns_domain.zone_id}"
  name = "${var.site_name}"
  type = "CNAME"
  records = ["${aws_cloudfront_distribution.www-fe.domain_name}"]
  ttl = "5"
}
