variable "env" {}

variable "s3_bucket" {}

variable "s3_bucket_arn" {}

variable "s3_bucket_id" {}

# variable "app" {
#   default = "demo"
# }

variable "waf_ip_whitelist" {
  type = list(map(string))
}

variable "root_object" {
  default = "index.html"
}

variable "site_name" {
  default = "hello"
}

variable "dns_domain" {
  default = "srce-test.ucdavis.edu"
}

data "aws_route53_zone" "dns_domain" {
  name = "${var.dns_domain}"
}
