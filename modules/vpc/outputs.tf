# Output VPC info for consumption by other configs

output "info" {
  value = {
    id                   = aws_vpc.standard.id
    vpc_name             = var.name
    vpc_cidr             = aws_vpc.standard.cidr_block
    vpc_public_nacl      = aws_network_acl.public.id
    vpc_private_nacl     = aws_network_acl.private.id
    ssh_sg               = aws_security_group.ssh_sg.id
    vpc_out_sg           = aws_security_group.vpc_out_sg.id
    www_out_sg           = aws_security_group.www_out_sg.id
    ec2_sg               = aws_security_group.ec2_sg.id
    alb_sg               = aws_security_group.alb_sg.id
    db_sg                = aws_security_group.db_sg.id
    alb_http_in_sg       = aws_security_group.alb_http_in_sg.id
    vpc_private_route    = aws_route_table.private.id
    subnet_ids_private   = "${join(",", aws_subnet.private.*.id)}"
    subnet_cidrs_private = join(",", aws_subnet.private.*.cidr_block)
    vpc_public_route     = aws_route_table.public.id
    subnet_ids_public    = "${join(",", aws_subnet.public.*.id)}"
    subnet_cidrs_public  = join(",", aws_subnet.public.*.cidr_block)
    db_subnet_group      = aws_db_subnet_group.rds.id
  }
}
