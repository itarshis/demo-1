# Locals
locals {
  creator = "Terraform"
}

# Module Variables
variable "env" {
  type = string
  default = "test"
}

variable "name" {
  type    = string
  default = "demo"
}

variable "cidr" {
  type    = string
  default = "10.10.0.0/16"
}

variable "subnet_public_newbits" {
  type        = number
  default     = 8
  description = "e.g. If set to 8, CIDR /16 -> /24"
}

variable "subnet_private_newbits" {
  type        = number
  default     = 8
  description = "e.g. If set to 8, CID /16 -> /24"
}

variable "number_of_subnets" {
  type        = number
  default     = 2
  description = "e.g. If set to 2, creates 2 public and 2 private subnets"
}

variable "nacl_public_ports" {
  type        = list(number)
  default     = [80, 443, 32]
  description = "Creates public ingress tcp ports, one for each in the list"
}

variable "http_public_ports" {
  type = list(string)
  default = [
    "80",
    "443"
  ]
}

variable "ucd_campus_cidrs" {
  type = list(string)
  default = [
    "168.150.0.0/18",
    "168.150.96.0/20",
    "168.150.112.0/21",
    "169.237.0.0/16",
    "128.120.0.0/16",
    "169.237.233.96/27"
  ]
  description = "Give access to the defined CIDRs above"
}

variable "custom_cidrs" {
  type        = list(string)
  default     = []
  description = "Give access to CIDRs defined in this list"
}

variable "vpc_config" {
  description = "Only change if absolutely needed"
  default = {
    rule_number_default = 100
    rule_number_public  = 1000
    rule_number_private = 2000
  }
}

variable "vpc_endpoint_service_name" {
  type = string
  default = null
}

variable "domain_name" {
  type = string
  default = null
}

variable "domain_servers" {
  type = list(string)
  default = null
}

variable "vpc_endpoint" {
  type = bool
  default = false
}

variable "domain_dhcp" {
  type = bool
  default = false
}

variable "store_in_ssm" {
  type = bool
  default = false
}

# Data Sources

data "aws_availability_zones" "available" {}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}
