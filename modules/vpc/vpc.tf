resource "aws_vpc" "standard" {
  cidr_block           = var.cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  tags = {
    Name         = "${var.name}-${var.env}"
    Stage        = var.env
    Createdby    = local.creator
  }
}

