# Configure the environment tag

variable "env" {
  type = string
  default = "dev"
}

# Configure a project name for our resources
variable "bucket_name" {
  type = string
  default = "proservices-demo"
}

variable "site_name" {
  type = string
  default = "hello"
}

variable "store_in_ssm" {
  type = string
  default = false
}

variable "dns_domain" {
  type = string
  default = "srce-test.ucdavis.edu"
}


variable "waf_ip_whitelist" {
  default = [
    { value = "67.187.240.136/32", type = "IPV4" }
  ]
}
