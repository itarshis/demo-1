module "data_bucket" {
  source = "../modules/s3"
  env = "${var.env}"
  bucket_name = "${var.site_name}-data"
  store_in_ssm = true
}

module "log_bucket" {
  source = "../modules/s3"
  env = "${var.env}"
  bucket_name = "${var.site_name}-logs"
  store_in_ssm = true
}

resource "aws_s3_bucket_object" "index" {
  bucket = "${module.data_bucket.info["bucket_id"]}"
  key = "index.html"
  source = "./files/index.html"
  etag = "${filemd5("./files/index.html")}"
  content_type = "text/html"
}

module "cloudfront-fe" {
  source = "../modules/cloudfront"
  env = "${var.env}"
  s3_bucket = "${module.data_bucket.info["bucket_dn"]}"
  s3_bucket_arn = "${module.data_bucket.info["bucket_arn"]}"
  s3_bucket_id = "${module.data_bucket.info["bucket_id"]}"
  waf_ip_whitelist = "${var.waf_ip_whitelist}"
  root_object = "index.html"
  dns_domain = "${var.dns_domain}"
}
