module "kms" {
  source = "../modules/kms"
  env = "${var.env}"
}

module "vpc" {
  source = "../modules/vpc"
  cidr = "${var.vpc_cidr}"
  name = "${var.env}-${var.app}"
  env = "${var.env}"
  store_in_ssm = true
  http_public_ports = "${var.http_public_ports}"
}

module "log_bucket" {
  source = "../modules/s3"
  env = "${var.env}"
  bucket_name = "${var.app}-alb-logs"
  store_in_ssm = true
}

module "alb" {
  source = "../modules/alb"
  env = "${var.env}"
  app = "${var.app}-${var.site_name}"
  vpc = "${module.vpc.info}"
  name = "${var.env}-${var.app}-alb"
  alb_sg = ["${module.vpc.info["alb_sg"]}"]
  dns_zone_id = "${data.aws_route53_zone.dns_domain.zone_id}"
  alb_record_name = "${var.site_name}.${var.dns_domain}"
}

module "web_instance" {
  source = "../modules/ec2"
  env = "${var.env}"
  app = "${var.app}-${var.site_name}"
  ssh_key = "ira-demo-06042020"
  ebs_kms = "${module.kms.export["ebs_kms_arn"]}"
  volume_size = 20
  web_text = "${var.web_text}"
  sgs = ["${module.vpc.info["ssh_sg"]}", "${module.vpc.info["www_out_sg"]}", "${module.vpc.info["alb_http_in_sg"]}"]
  image_id = "${data.aws_ami.amazon_linux.id}"
  asg_name = "${var.env}-${var.app}-${var.site_name}-asg"
  asg_vpc_zones = ["${module.vpc.info["subnet_ids_private"]}"]
  ec2_type = "t2.small"
  target_group_arn = ["${module.alb.info["target_group_arn"]}"]
}
