provider "aws" {
	version = "~> 2.38.0"
	region = "us-east-2"
}


# This portion should be commented out until a local state file is created

# To escablish a remote state file
#		1) Run "terraform apply" with the two block bellow commented out to create a local tfstate file
#		2) Uncomment the "terraform" block below
#		3) Run "terraform apply" and say "yes" to have the tfstate file copied to s3
#		4) Uncomment the "data" block bellow
#		5) Delete (or rename) the local tfstate file
#		6) Run "terraform apply" and make sure the remote state is being used

terraform {
	required_version = "<= 0.12.12"
	backend "s3" {
		bucket  = "proservices-demo"
	 	key     = "dev/demo.tfstate"
		region  = "us-west-2"
	}
}

# This portion should be commented out until a remote state file is established

# data "terraform_remote_state" "demo" {
#   backend = "s3"
#   config = {
# 		bucket  = "proservices-demo"
#     key     = "demo/website.tfstate"
#     region = "us-west-2"
#   }
# }
