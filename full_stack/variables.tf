data "aws_ami" "amazon_linux" {
  most_recent = true
  owners = ["amazon"]
  filter {
    name = "owner-alias"
    values = ["amazon"]
  }
  filter {
    name = "name"
    values = ["amzn-ami-hvm-*-x86_64-gp2"]
  }
}

variable "env" {
  type = string
  default = "demo"
}

variable "vpc_cidr" {
  type = string
  default = "10.30.0.0/16"
}

variable "app" {
  type = string
  default = "ps"
}

variable "http_public_ports" {
  type = list(string)
  default = [
    "80",
    "443"
  ]
}

variable "site_name" {
  type = string
  default = "hello"
}

variable "web_text" {
  type = string
  default = "Hello World!"
}

variable "dns_domain" {
  type = string
  default = "srce-test.ucdavis.edu"
}

data "aws_route53_zone" "dns_domain" {
  name = "${var.dns_domain}"
}
