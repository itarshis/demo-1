# Create a key for encrypting our S3 bucket

resource "aws_kms_key" "s3_encryption" {
  description = "${var.env} KMS key for encrypting S3 bucket"
  tags = {
    Name = "${var.env}-${var.bucket_name}-s3-encryption-key"
    CreatedBy = "Terraform"
    STAGE = "${var.env}"
  }
}

# Create a random string as a lazy way to make our S3 bucket names unique

resource "random_string" "s3_tail" {
  length = 5
  special = false
  upper = false
}

# Create S3 Bucket using our random string to ensure that it's unique

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${var.env}-${var.bucket_name}-${random_string.s3_tail.result}"
  acl    = "private"
  tags = {
    Environment = "${var.env}"
    CreatedBy   = "Terraform"
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
        kms_master_key_id = "${aws_kms_key.s3_encryption.id}"
      }
    }
  }
}

# Stick the name of our S3 bucket into SSM

resource "aws_ssm_parameter" "standard" {
    count = var.store_in_ssm ? 1 : 0
    name = "${var.env}-${var.bucket_name}-s3-bucket"
    type = "String"
    value = "${aws_s3_bucket.s3_bucket.id}"
}
