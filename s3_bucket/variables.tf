# Configure the environment tag

variable "env" {
  type = string
  default = "dev"
}

# Configure a project name for our resources
variable "bucket_name" {
  type = string
  default = "proservices-demo"
}

variable "store_in_ssm" {
  type = string
  default = true
}
