# README #

Brief instructions on using this demo repository. Feel free to take this code and do whatever you want with it.

### What is this repository for? ###

* Contains demo deployments used during Terraform ProServices Lightning Talk
	* S3 Bucket
	* EC2 Web Server and VPC (requires DNS zone in Route 53)
	* S3 static site and Cloudfront (requires DNS zone in Route 53)

### How do I get set up? ###

* Install Terraform: https://learn.hashicorp.com/terraform/getting-started/install.html
* Download the Repo
* Open a shell that can invoke Terraform
* Browse to the directory containing the demos
* Load AWS credentials into your shell session
* Configure the variables.tf file or create a .tfvars file to overwrite the default variable values
* Run 'terraform init' in the directory of the demo you want to run
* Run 'terraform apply'
* Check the output to ensure that all tasks look correct
* Check your AWS account to validate the apply worked

### Who do I talk to? ###

* itarshis@ucdavis.edu